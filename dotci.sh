#!/bin/bash
. ~/.config/dotci.sh

IFS=";" read -ra ADDR <<< "$ci_dirs"
for i in "${ADDR[@]}"; do
    IFS=":" read -ra ADDR <<< "$i"
    export ci_dir_${ADDR[0]}=${ADDR[1]}
done

ci_dir_cd() {
    cd $(printenv ci_dir_$1)
}
