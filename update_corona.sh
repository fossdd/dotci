#!/bin/bash
DATE=$(date +%s)

. ~/.dotci.sh

ci_dir_cd corona

git config --global pull.rebase true # Force rebase

# impfdashboard.de
if [ -d "impfdashboard.de" ]; then
  cd impfdashboard.de
  git pull
  # rm -fr data
  time ./src/download.sh
  git add data
  git commit -m "$DATE"
  git push origin
  git push github
  git push srht
  cd ..
fi

# soziales.hessen.de
if [ -d "soziales.hessen.de" ]; then
  cd soziales.hessen.de
  git pull
  # rm -fr data
  mkdir -p data
  time cargo run --release
  mv out.csv data/parsed.csv
  git add data
  git commit -m "$DATE"
  git push origin
  git push github
  git push srht
  cd ..
fi

# stadt-koeln.de
if [ -d "stadt-koeln.de" ]; then
  cd stadt-koeln.de
  git pull
  # rm -fr data
  output=$(cargo run --release 2>&1)
  echo $output
  [[ "$output" = *"new table"* ]] && [[ $(curl -X GET "https://codeberg.org/api/v1/repos/corona/stadt-koeln.de/issues?state=open&q=New%20table%20by%20upstream&type=issues&page=1&limit=1&access_token=${CODEBERG_ACCESS_TOKEN}" -H 'accept: application/json') == "[]" ]] && curl -X POST "https://codeberg.org/api/v1/repos/corona/stadt-koeln.de/issues?access_token=${CODEBERG_ACCESS_TOKEN}" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"body\": \"This is a automatically report of the production update system.\n\nThe source code found a new table at the \`stadt-koeln.de\`'s website.\nIt could be that the order has been changed, so it needes manual verification.\", \"labels\": [0], \"title\": \"New Table by upstream\"}"
  mkdir -p data/parsed
  mv *.csv data/parsed
  git add data
  git commit -m "$DATE"
  git push origin
  git push github
  git push srht
  cd ..
fi

# who.int
if [ -d "who.int" ]; then
  cd soziales.hessen.de
  git pull
  # rm -fr data
  time ./src/download.sh 
  git add data
  git commit -m "$DATE"
  git push origin
  git push github
  git push srht
  cd ..
fi

# intensivregister.de
if [ -d "intensivregister.de" ]; then
  cd intensivregister.de
  git pull
  # rm -fr data
  time ./src/download.sh 
  git add data
  git commit -m "$DATE"
  git push origin
  git push github
  git push srht
  cd ..
fi

# dl corona-urls
if [ -d "corona-urls" ]; then
  cd corona-urls
  git pull
  cd ..

  # http_data
  DIR="http_data"
  [ ! -d "$DIR" ] && mkdir "$DIR" && cd "$DIR" && git init && cd ..
  # cd "$DIR"
  # shopt -s extglob
  # rm -fr !(.git)
  # cd ..
  while IFS= read -r line; do
    echo $line
    OUT="$DIR/${line:8}"
    if [ "${line:0:1}" != "#" ]; then
      timeout 30 curl -L "$line" -o "$OUT" --create-dirs || rm -f $OUT
    fi
  done < corona-urls/http_urls
  cd $DIR
  git add .
  git commit -m "$DATE"
  cd ..

  # git_data
  while IFS= read -r line; do
    echo $line
    OUT="git_data/${line:8}"
    if [ "${line:0:1}" != "#" ]; then
      if [ -d "$OUT" ]; then
        pushd $OUT
        git fetch --all
        git pull --all
        popd
      else
        git clone "$line" "$OUT"
      fi
    fi
  done < corona-urls/git_urls
fi
