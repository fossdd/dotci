#!/bin/bash
. ~/.dotci.sh
ci_dir_cd fdroiddata

CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)

DATE=$(date -u +%s)
COMMAND="./tools/check-fastlane-as-fallback.py"
BRANCH="fossdd-check-fastlane-as-fallback-$DATE"

OUTPUT=$(time $COMMAND)

git checkout -b "$BRANCH"
git add .
git commit -m "$COMMAND" -m "$OUTPUT"
git push --set-upstream origin "$BRANCH"
git checkout "$CURRENT_BRANCH"

SOURCE_PROJECT="23780947" # fossdd/fdroiddata
SOURCE_BRANCH=$BRANCH
TARGET_PROJECT="36528" # fdroid/fdroiddata
TARGET_BRANCH="master"
TITLE="Update Fastlane as Fallback"
DESCRIPTION="Upstream of these apps had added fastlane to their repo. However, we don't pick these metadata cause in the build recipe is the description overwritten. This MR removes the Description, and also enables fastlane support.\nThis is a automatically created MR, means that errors could ocurr."

curl \
    -X POST \
    -H "Content-Type: application/json" \
    -H "PRIVATE-TOKEN: $GITLAB_SECRET" \
    "https://gitlab.com/api/v4/projects/$SOURCE_PROJECT/merge_requests" \
    --data "{\"source_branch\": \"$SOURCE_BRANCH\", \"target_branch\": \"$TARGET_BRANCH\", \"title\": \"$TITLE\", \"description\": \"$DESCRIPTION\", \"target_project_id\": \"$TARGET_PROJECT\"}"
