#!/bin/bash
. ~/.dotci.sh
ci_dir_cd fdroid_root

rebase() {
    git fetch upstream
    git checkout master
    git rebase upstream/master
    git push origin master
}

dir() {
    pushd "${1}"
    rebase
    popd
}

dir data-fossdd-updatingfork
dir issuebot-fossdd-updatingfork
dir website-fossdd-updatingfork
dir server-fossdd-updatingfork
dir ci-images-base-fossdd-updatingfork
dir client-fossdd-updatingfork
dir website-search-fossdd-updatingfork
dir fdroid-ci-images-client-updatingfork
